//import model / mongoose
const { Course } = require('../models/courseModel');
const mongoose = require('mongoose');


const CourseController = {
    //GET all courses
    getAllCourses : (req,res) => {
        Course.find( (err,data) => {
            if (err) {
                return res.status(500).json({message:`Internal server error: ${err.message}`})
            } else {
                return res.status(200).json(data)
            }
        })
    },
    //POST a course
    createCourse : (req,res) => {
        let bodyReq = req.body;
            let newCourse = {
                _id: mongoose.Types.ObjectId(),
                courseCode: bodyReq.courseCode,
                courseName:  bodyReq.courseName,
                price: bodyReq.price,
                discountPrice: bodyReq.discountPrice,
                level: bodyReq.level,
                duration: bodyReq.duration,
                coverImage: bodyReq.coverImage,
                teacherName: bodyReq.teacherName,
                teacherPhoto: bodyReq.teacherPhoto,
                isPopular: bodyReq.isPopular,
                isTrending: bodyReq.isTrending
            };

            Course.create(newCourse, (err,data) => {
                if (err) {
                    return res.status(500).json({message:`Internal server error: ${err.message}`})
                } else {
                    return res.status(201).json(data)
                }
            });
    },

    //GET course by Id
    getCourseById:  async (req,res) => {
        try{
            const course = await Course.findById(req.params.courseId);
            res.status(200).json(course);
        }catch(err) {
            res.status(500).json({
                err: err.message
            });
        }
    },  

    //PUT course by Id
    updateCourseById:  (req,res) => {
        let courseId = req.params.courseId
        let bodyReq = req.body;
            let courseUpdate = {
                courseCode: bodyReq.courseCode,
                courseName:  bodyReq.courseName,
                price: bodyReq.price,
                discountPrice: bodyReq.discountPrice,
                level: bodyReq.level,
                duration: bodyReq.duration,
                coverImage: bodyReq.coverImage,
                teacherName: bodyReq.teacherName,
                teacherPhoto: bodyReq.teacherPhoto,
                isPopular: bodyReq.isPopular,
                isTrending: bodyReq.isTrending
            };

        if (!mongoose.Types.ObjectId.isValid(courseId)) {
            return res.status(400).json({
                status: 'ERROR 400: Bad Request',
                message: 'Course Id is invalid!'
            })
        };

        if (courseUpdate.price < 0 || courseUpdate.discountPrice < 0 || Number(courseUpdate.price) < Number(courseUpdate.discountPrice)) {
            return res.status(400).json({
                status: 'ERROR 400: Bad Request',
                message: 'Price or Discount Price are not valid'
            });
        } 
        else {
            Course.findByIdAndUpdate(courseId, courseUpdate, (err,data) => {
                if (err) {
                    return res.status(500).json({message:`Internal server error: ${err.message}`})
                } else {
                    return res.status(200).json(data)
                }
            })
        }
        
    },  
   
    //DELETE course by Id
    deleteCourseById:  async (req,res) => {
        try{
            const course = await Course.findByIdAndDelete(req.params.courseId);
            res.status(204).json(course);
        }catch(err) {
            res.status(500).json({
                err: err.message
            });
        }
    },  
};


module.exports =  CourseController;
