//import mongooseJS
const mongoose = require('mongoose');

//khởi tạo schema
const courseSchema = new mongoose.Schema({
    _id: {
        type: mongoose.Types.ObjectId,
    },
    courseCode: {
        type: String,
        required: true,
        unique: true,
    },
    courseName: {
        type: String,
        required: true,
    },
    price: {
        type: Number,
        required: true,
    },
    discountPrice: {
        type: Number,
        required: true,
    },
    level: {
        type: String,
        required: true,
    },
    duration: {
        type: String,
        required: true,
    },
    coverImage: {
        type: String,
        required: true,
    },
    teacherName: {
        type: String,
        required: true,
    },
    teacherPhoto: {
        type: String,
        required: true,
    },
    isPopular: {
        type: Boolean,
        default: true,
    },
    isTrending: {
        type: Boolean,
        default: false,
    },
    ngayTao: {
        type: Date,
        default: Date.now(),
    },
    ngayCapNhat: {
        type: Date,
        default: Date.now(),
    },
});

//export ra module
let Course = mongoose.model('Course', courseSchema);
module.exports = { Course }
