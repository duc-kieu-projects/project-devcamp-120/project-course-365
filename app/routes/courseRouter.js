 //import express
const courseRouter = require('express').Router();

//import controller 
const CourseController = require('../controllers/courseController');

//get all courses
courseRouter.get('/courses', CourseController.getAllCourses);

//get course by Id
courseRouter.get('/courses/:courseId', CourseController.getCourseById);

//create a new course
courseRouter.post('/courses', CourseController.createCourse);

//update a course by Id
courseRouter.put('/courses/:courseId', CourseController.updateCourseById);

//delete a course by Id
courseRouter.delete('/courses/:courseId', CourseController.deleteCourseById);

//exports 
module.exports = courseRouter;