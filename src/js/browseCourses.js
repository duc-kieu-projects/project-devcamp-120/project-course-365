"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    const gCOL_NAME = ["courseCode", "courseName", "coverImage", "price", "discountPrice",
                     "duration", "level", "teacherName", "teacherPhoto", "isPopular", "isTrending", "action"];
    const gCOL_COURSE_CODE = 0;
    const gCOL_COURSE_NAME = 1;
    const gCOL_COVER_IMAGE = 2;
    const gCOL_PRICE = 3;
    const gCOL_DISCOUNT_PRICE = 4;
    const gCOL_DURATION = 5;
    const gCOL_LEVEL = 5;
    const gCOL_TEACHER_NAME = 7;
    const gCOL_TEACHER_PHOTO = 8;
    const gCOL_POPULAR = 9;
    const gCOL_TRENDING = 10;
    const gCOL_ACTION = 11;

    //biến lưu trữ ID
    var gId = null;

    //biến lưu dữ liệu tạo course/update course
    var gCourse = {
        courseCode: "",
        courseName: "",
        price: -1,
        discountPrice: -1,
        duration: "",
        level: "",
        coverImage: "",
        teacherName: "",
        teacherPhoto: "",
        isPopular: "",
        isTrending: "",
    };
    
    //mảng lưu dữ liệu
    var gListCourses = [];

    //biến lưu dữ liệu row index
    var gRowIndex = -1;



/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    $(document).ready(function(){
        $("#danger-alert").hide();
        ajaxGetAllCourses();
        //sự kiện click icon Add Course
        $(document).on('click', '.btn-outline-success', function(){
            $('#modal-insert-course').modal("show");
        })
        //sự kiện click nút Insert trong modal Add Course
        $(document).on('click', '.btn-insert-course', function(){
            onBtnInsertCourseClick();
        })
        //sự kiện click icon Update Course
        $(document).on('click', '.fa-edit', function(){
            onBtnUpdateCourseClick(this);
        })
        //sự kiện click nút Update trong modal Update Course
        $(document).on('click', '#btn-update-course', function(){
            onBtnUpdateModalClick();
        })
        //sự kiện click icon Delete 
        $(document).on('click', '.fa-trash', function(){
            onBtnDeleteCourseClick(this);
        })
        //sự kiện click nút Xác Nhận trong modal Delete Course
        $(document).on('click', '#btn-delete-course', function(){
            onBtnDeleteModalClick();
        })
    })
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    //hiển thị dữ liệu khóa học ra bảng
    function loadDataToTable(paramResponse){
        "use strict";
        var gTable = $('#table-course').DataTable({
            columns: [
                { data:gCOL_NAME[gCOL_COURSE_CODE] },
                { data:gCOL_NAME[gCOL_COURSE_NAME] },
                { data:gCOL_NAME[gCOL_COVER_IMAGE] },
                { data:gCOL_NAME[gCOL_PRICE] },
                { data:gCOL_NAME[gCOL_DISCOUNT_PRICE] },
                { data:gCOL_NAME[gCOL_DURATION] },
                { data:gCOL_NAME[gCOL_LEVEL] },
                { data:gCOL_NAME[gCOL_TEACHER_NAME] },
                { data:gCOL_NAME[gCOL_TEACHER_PHOTO] },
                { data:gCOL_NAME[gCOL_POPULAR] },
                { data:gCOL_NAME[gCOL_TRENDING] },
                { data:gCOL_NAME[gCOL_ACTION] },
            ],
            columnDefs: [
                {
                  targets: gCOL_COVER_IMAGE,
                  className: "text-center",
                  render: function (data, type, row, meta) {
                      return '<img class="img-thumbnail" src="'+ data +'"/>';
                  }  
                },
                {
                    targets: gCOL_PRICE,
                    render: function (data) {
                        return data + "$";
                    }
                },
                {
                    targets: gCOL_DISCOUNT_PRICE,
                    render: function (data) {
                        return data + "$";
                    }
                },
                {
                    targets: gCOL_TEACHER_PHOTO,
                    className: "text-center",
                    render: function (data, type, row, meta) {
                        return '<img class="img-thumbnail" src="'+ data +'" height="90" width="90"/>';
                    }  
                  },
                {
                    targets: gCOL_ACTION,
                    defaultContent: `<i class="fas fa-edit fa-lg text-primary mx-2" data-toggle="tooltip" title="Update Course"></i>
                    <i class="fas fa-trash fa-lg text-danger" data-toggle="tooltip" title="Delete Course"></i>`
                },
            ],
        });        
        gTable.clear();
        gTable.rows.add(paramResponse).draw();
    };
    //hàm xử lý khi click vào nút Insert trong modal Insert Course
    function onBtnInsertCourseClick(){
        "use strict";
        console.log("Clicked Add Course");
        //B1: khai báo dữ liệu
        getDataCourse(gCourse);
        //B2: Kiểm tra dữ liệu
        var vCheck = validateDataCourse(gCourse);
        if (vCheck) {
        //B3: Xử lý dữ liệu
            $.ajax({
                url: '/devcamp-course365/courses',
                type: 'POST',
                data: JSON.stringify(gCourse),
                dataType: 'json',
                contentType: "application/json;charset=UTF-8",
                success: function(response) {
                    console.log(response);
                    gListCourses.push(response);
                    $("#newCourseID").html(response.courseCode)
                    $("#modal-success").modal("show");
                    showDataToTable(gListCourses);
                },
                error: function(err) {
                    console.log(err.responseText);
                }
            })
            //reset modal insert course
            resetModalInsert();
            //ẩn modal insert course
            $('#modal-insert-course').modal("hide");
        }       
    }
    //hàm xử lý khi click icon Update Course
    function onBtnUpdateCourseClick(paramIcon){
        "use strict";
        $('#modal-update-course').modal("show");
        var vRowClick = $(paramIcon).closest("tr");
        var vTable = $('#table-course').DataTable();
        gRowIndex = vTable.row(vRowClick).index();
        var vDataRow = vTable.row(vRowClick).data();
        gId = vDataRow._id;
        console.log(gId);
        console.log(vDataRow);
        displayUpdateCourseModal(vDataRow);
    }
    //hàm xử lý khi click icon Delete Course
    function onBtnDeleteCourseClick(paramButtonCurrent){
        "use strict";
        $('#modal-delete-course').modal("show");
        var vRowClick = $(paramButtonCurrent).closest('tr');
        var vTable = $('#table-course').DataTable();
        var vDataRow = vTable.row(vRowClick).data();
        gRowIndex = vTable.row(vRowClick).index();
        gId = vDataRow._id;
        console.log(vDataRow);
    }
    //hàm xử lý khi click vào nút Update trong modal Update Course
    function onBtnUpdateModalClick() {
        "use strict";
        console.log("Update Clicked");
            //B1: đọc dữ liệu
            getDataUpdateCourse(gCourse);
            //B2: kiểm tra dữ liệu
            var vCheck = validateDataCourseUpdate(gCourse);
            if ( vCheck ) {
                //B3:xử lý dữ liệu
                 //update course
                 $.ajax({
                    url: '/devcamp-course365/courses/' + gId,
                    type: 'PUT',
                    async: false,
                    data: JSON.stringify(gCourse),
                    dataType: 'json',
                    contentType: "application/json;charset=UTF-8",
                    success: function(response) {
                        updateCourse(response)
                    },
                    error: function(err) {
                        console.log(err.responseText);
                    }
                 })
                //reset modal update
                resetModalUpdate();
                //ẩn modal update
                $('#modal-update-course').modal("hide");
            }
    }
    //hàm xử lý khi click nút Xác Nhận trong modal Delete 
    function onBtnDeleteModalClick() {
        "use strict";
        //B1: đọc dữ liệu
        //B2: kiểm tra dữ liệu
        //B3: xóa course
        $.ajax({
            url: '/devcamp-course365/courses/' + gId,
            type: 'DELETE',
            async: false,
            success: function(response) {
                var vTable = $('#table-course').DataTable();
                vTable.row(gRowIndex).remove().draw();
                //ẩn modal delete
                $('#modal-delete-course').modal("hide");
                showAlert()
            },
            error: function(err) {
                alert(err.responseText)
            }
        })
    }
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    
        //hàm ajax get all courses
        function ajaxGetAllCourses() {
            $.ajax({
                url: '/devcamp-course365/courses',
                type: 'GET',
                success: function(response) {
                    console.log(response);
                    gListCourses = response;
                    loadDataToTable(gListCourses)
                },
                error: function(err) {
                    alert(err.responseText)
                }
            })
        }


//*****----------------------vùng xử lý Insert Course----------------------------*****//
        //hàm đọc dữ liệu
        function getDataCourse(gNewCourse) {
            "use strict";
            gNewCourse.courseCode = $('#inp-course-code').val().trim();
            gNewCourse.courseName = $('#inp-course-name').val().trim();
            gNewCourse.price = $('#inp-price').val();
            gNewCourse.discountPrice = $('#inp-discountPrice').val();
            gNewCourse.duration = $('#inp-duration').val().trim();
            gNewCourse.level = $('#inp-level').val().trim();
            gNewCourse.coverImage = $('#inp-image').val().trim();
            gNewCourse.teacherName = $('#inp-teacher-name').val().trim();
            gNewCourse.teacherPhoto = $('#inp-teacher-photo').val().trim();
            gNewCourse.isPopular =  $('#select-popular').val();
            gNewCourse.isTrending = $('#select-trending').val();
    }
        
        //hàm kiểm tra dữ liệu
        function validateDataCourse(paramData) {
            "use strict";
            if ( checkCourseCode(paramData.courseCode) ) {
                alert('[Course Code] existed');
                return false;
            };

            if ( paramData.courseCode == "") {
                alert("[Course Code] must be filled");
                return false;
            };

            if ( paramData.courseName == "" ){
                alert("[Course Name] must be filled");
                return false;
            };

            if ( !(Number(paramData.price) > 0 
                && Number(paramData.discountPrice)  >= 0) ) {
                alert ('[Price and Discount Price] are invalid');
                return false;
            };

            if (Number(paramData.price) < Number(paramData.discountPrice)) {
                alert('Discount Price are not greater than Price');
                return false;
            };

            if ( paramData.duration == "" ){
                alert("[Duration] must be filled");
                return false;
            };

            if ( paramData.level == "" ){
                alert("[Level] must be filled");
                return false;
            };
            if ( paramData.coverImage == "" ){
                alert("[Cover Image] must be filled");
                return false;
            };

            if ( paramData.teacherName == "" ){
                alert("[Teacher Name] must be filled");
                return false;
            };

            if ( paramData.teacherPhoto == "" ){
                alert("[Teacher Photo] must be filled");
                return false;
            };

            return true;
        }

        //hàm kiểm tra mã khóa học
        function checkCourseCode(paramCheck){
            "use strict";
            var vIndex = 0;
            var vFound = false;
            var vResult = false;
            while(!vFound && vIndex < gListCourses.length) {
                if (paramCheck == gListCourses[vIndex].courseCode) {
                    vFound = true;
                    vResult = true;
                }
                vIndex++;
            }
            return vResult;
        }

        //hàm hiển thị khóa học mới lên bảng
        function showDataToTable(paramCourse) {
            "use strict";
            var vTable = $('#table-course').DataTable();
            vTable.clear();
            vTable.rows.add(paramCourse);
            vTable.draw();
        }

        //hàm reset modal Insert
        function resetModalInsert() {
            "use strict";
            $('#inp-course-code').val("");
            $('#inp-course-name').val("");
            $('#inp-price').val("");
            $('#inp-discountPrice').val("");
            $('#inp-duration').val("");
            $('#inp-level').val("");
            $('#inp-image').val("");
            $('#inp-teacher-name').val("");
            $('#inp-teacher-photo').val("");
            $('#select-popular').val("");
            $('#select-trending').val("");
        }



//*****-------------------vùng xử lý Update Course----------------------------- *****//
        //hàm hiển thị update course modal
        function displayUpdateCourseModal(paramData) {
            'use strict';
            $('#inp-course-code-update').val(paramData.courseCode);
            $('#inp-course-name-update').val(paramData.courseName);
            $('#inp-price-update').val(paramData.price);
            $('#inp-discountPrice-update').val(paramData.discountPrice);
            $('#inp-duration-update').val(paramData.duration);
            $('#inp-level-update').val(paramData.level);
            $('#inp-image-update').val(paramData.coverImage);
            $('#inp-teacher-name-update').val(paramData.teacherName);
            $('#inp-teacher-photo-update').val(paramData.teacherPhoto);
            $('#inp-popular-update').val(paramData.isPopular);
            $('#inp-trending-update').val(paramData.isTrending);
        }

        //hàm đọc dữ liệu
        function getDataUpdateCourse(paramDataUpdate) {
            "use strict";
            paramDataUpdate.courseCode = $('#inp-course-code-update').val().trim();
            paramDataUpdate.courseName = $('#inp-course-name-update').val().trim();
            paramDataUpdate.price = $('#inp-price-update').val();
            paramDataUpdate.discountPrice = $('#inp-discountPrice-update').val();
            paramDataUpdate.duration = $('#inp-duration-update').val().trim();
            paramDataUpdate.level = $('#inp-level-update').val().trim();
            paramDataUpdate.coverImage = $('#inp-image-update').val().trim();
            paramDataUpdate.teacherName = $('#inp-teacher-name-update').val().trim();
            paramDataUpdate.teacherPhoto = $('#inp-teacher-photo-update').val().trim();
            paramDataUpdate.isPopular = $('#inp-popular-update').val();
            paramDataUpdate.isTrending = $('#inp-trending-update').val();
        }
        //hàm kiểm tra dữ liệu
        function validateDataCourseUpdate(paramData) {
            "use strict";
            if ( paramData.courseCode == "") {
                alert("[Course Code] must be filled");
                return false;
            };

            if ( paramData.courseName == "" ){
                alert("[Course Name] must be filled");
                return false;
            };

            if ( !(Number(paramData.price) > 0 
                && Number(paramData.discountPrice) >= 0) ) {
                alert ('[Price and Discount Price] are invalid');
                return false;
            };

            if (Number(paramData.price) < Number(paramData.discountPrice)) {
                alert('Discount Price are not greater than Price');
                return false;
            }

            if ( paramData.duration == "" ){
                alert("[Duration] must be filled");
                return false;
            };

            if ( paramData.level == "" ){
                alert("[Level] must be filled");
                return false;
            };
            if ( paramData.coverImage == "" ){
                alert("[Cover Image] must be filled");
                return false;
            };

            if ( paramData.teacherName == "" ){
                alert("[Teacher Name] must be filled");
                return false;
            };

            if ( paramData.teacherPhoto == "" ){
                alert("[Teacher Photo] must be filled");
                return false;
            };

            if ( paramData.isPopular == "" || paramData.isPopular == null ){
                alert("[Popular] must be selected");
                return false;
            }; 
            
            if ( paramData.isTrending == "" || paramData.isTrending == null ){
                alert("[Trending] must be selected");
                return false;
            };
            return true;
        }
       
        //hàm xử lý hiển thị course update
        function updateCourse(paramResponse) {
            'use strict';
            $.ajax({
                url: '/devcamp-course365/courses/' + gId,
                type: 'GET',
                async: false,
                success: function(response) {
                    var vTable = $('#table-course').DataTable();
                    vTable.row(gRowIndex).data(response).draw();
                },
                error: function(err) {
                    alert(err.responseText)
                }
            })
        };

        //hàm reset modal Update Course
        function resetModalUpdate() {
            "use strict";
            $('#inp-course-code-update').val("");
            $('#inp-course-name-update').val("");
            $('#inp-price-update').val("");
            $('#inp-discountPrice-update').val("");
            $('#inp-duration-update').val("");
            $('#inp-level-update').val("");
            $('#inp-image-update').val("");
            $('#inp-teacher-name-update').val("")
            $('#inp-teacher-photo-update').val("");
            $('#inp-popular-update').val("");
            $('#inp-trending-update').val("");
        }


//*****--------------------------vùng xử lý Delete Course-------------------------- *****//
        
        //hàm show alert
        function showAlert() {
            $("#danger-alert").fadeTo(2000, 500).slideUp(500, function() {
            $("#danger-alert").slideUp(500);
            });
        }
        
